## **README**

Este repositório contém os scripts necessários para fazer `build` das imagens `hudson-shared/rstudio/base` e `hudson-shared/rstudio/latex`. A imagem base é usada para tarefas comuns de análise de dados enquanto a segunda para criação de artigos em LaTeX via [RStudio](https://www.rstudio.com/).

### **VERSÕES**

Esta é a versão inicial, com as seguintes opções:

* RStudio base:
    * `1.0.0-rstudiolatest-r3.4.3-java8-h2o3.16.0.3-debianjessie` que representa a versão da imagem (`1.0.0`), do RSutdio (`latest`), R ( `3.4.3`), Java (`8`) H2O (`3.16.0.3`) e Debian (`jessie`)
* RStudio latex:
    * `1.0.0-rstudiolatest-r3.4.3-java8-h2o3.16.0.3-debianjessie-latex`: que adiciona o LaTeX.
	
Para ter acesso a versões mais atuais, acesse a branch deste repositório que representará a versão de interesse.

### **PRÉ-REQUISITOS**

* **Ter executado com sucesso as instruções do respositório [environment_config](https://gitlab.com/hudson-shared/environment_config)**

### **RODANDO CONTAINERS**

As instruções para uso destas imagens está disponível no repositório [architecture_templates](https://gitlab.com/hudson-shared/architecture_templates). Lá você encontrará templates de uso das imagens separadamente ou em cojunto com outras tecnologias (RStudio, Jupyter, PostgreSQL, ...).

### **O QUE TEMOS NESTAS IMAGENS**

i) **rocker/tidyverse**: a imagem base do dockerfile, disponível do [docker hub](https://hub.docker.com/r/rocker/tidyverse/) que nos fornece uma versão do R juntamente com o RStudio.

ii) **Oracle Java 8**: instalado a partir repositório do site WebUpd8

iii) **Pacotes R do interesse**: estão instalados os seguintes pacotes:

* knitr
* rmarkdown
* rprojroot
* roxygen2
* testthat
* text2vec
* tidytext
* tm
* stringi
* SnowballC
* ptstem
* hunspell
* stringdist
* RJSONIO
* RCurl
* data.table
* plyr
* feather
* RPostgreSQL
* fst
* DT
* plotly
* dygraphs
* highcharter
* leaflet
* maps
* bit64
* MASS
* sparklyr
* properties
* flexdashboard
* shinydashboard
* shinythemes
* titanic
* geosphere

**Importante**: quando o container dessa imagem for construido de uma máquina virtual, é preciso utilizar o IP da vm. No caso da máquina construida a partir de [environment_config](https://gitlab.com/hudson-shared/environment_config) o endereço ip será 192.168.33.10. Caso executado em modo local, para descobrir o IP execute `ip addr | grep docker0`.