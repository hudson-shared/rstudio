#!/bin/bash

version='1.0.0-rstudiolatest-r3.4.3-java8-h2o3.16.0.3-debianjessie-latex'
image="registry.gitlab.com/hudson-shared/rstudio/latex:${version}"

docker build -t $image .

# Se o build foi com sucesso...Salva a versão atual para o processo de publicação usar a última imagem construída
if [ $? == 0 ]; then
    echo "$version" >.version 
fi 
